# ✔ 세미나 노트 
|강의|
|-------|
|[따라하며 배우는 노드, 리액트 시리즈 - 기본 강의](https://www.inflearn.com/course/%EB%94%B0%EB%9D%BC%ED%95%98%EB%A9%B0-%EB%B0%B0%EC%9A%B0%EB%8A%94-%EB%85%B8%EB%93%9C-%EB%A6%AC%EC%95%A1%ED%8A%B8-%EA%B8%B0%EB%B3%B8/dashboard)|
|[한입 크기로 잘라 먹는 리액트(React.js) : 기초부터 실전까지](https://www.inflearn.com/course/%ED%95%9C%EC%9E%85-%EB%A6%AC%EC%95%A1%ED%8A%B8)|

## 1. 따라하며 배우는 노드, 리액트 시리즈 - 기본 강의 
- [`20220725` - nodejs](./docs/node-react/20220725.md)
- [`20220726` - nodejs](./docs/node-react/20220726.md)
- [`20220727` - nodejs](./docs/node-react/20220727.md)
- [`20220727` - reactjs](./docs/node-react/20220727-react.md)
- [`20220728` - reactjs](./docs/node-react/20220728.md)

## 2. 한입 크기로 잘라 먹는 리액트(React.js) : 기초부터 실전까지
- [`20220818`](./docs/react/20220818.md)
- [React 실전 프로젝트](./docs/react/20220829.md)
